import asyncio
from pyppeteer import launch
import re
import requests
import json
import youtube_dl

ydl = youtube_dl.YoutubeDL({'outtmpl': '%(id)s%(ext)s'})

ydl_opts = {
    'nocheckcertificate': True,
}

url = 'http://127.0.0.1:5500/socials/youtube.html'


async def main():
    browser = await launch({'headless': True})
    page = await browser.newPage()
    await page.goto(url, {
        'waitUntil': 'load'
    })

    videoLinks = []

    for frameElement in page.mainFrame.childFrames:
        titleLink = await frameElement.querySelector('a.ytp-title-link')
        titleLinkHref = await titleLink.getProperty('href')
        videoLink = await titleLinkHref.jsonValue()
        videoLinks.append(videoLink)

    # print(videoLinks)
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download(videoLinks)

asyncio.get_event_loop().run_until_complete(main())
