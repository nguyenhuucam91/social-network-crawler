import asyncio
from pyppeteer import launch
import requests
import json

url = 'http://127.0.0.1:5500/socials/telegram.html'


async def main():
    browser = await launch({'headless': True})
    page = await browser.newPage()
    await page.goto(url, {
        'waitUntil': 'load',
        'timeout': 0

    })

    output = []

    for frameElement in page.mainFrame.childFrames:
        mergedVideoData = dict()
        photo = await getPhoto(frameElement)
        textContent = await getTextContent(frameElement)
        avatar = await getAvatar(frameElement)
        username = await getUsername(frameElement)
        mergedVideoData.update(avatar)
        mergedVideoData.update(username)
        mergedVideoData.update(photo)
        mergedVideoData.update(textContent)

        output.append(mergedVideoData)

    print(output)

    await browser.close()


async def getPhoto(frame):
    selector = ".tgme_widget_message_photo_wrap"
    photos = await frame.querySelectorAllEval(selector, '''
    element => {
        if (element.length > 0) {
            for (i = 0; i < element.length; i++) {
            const regexBackgroundUrl = /background[-]image:url\(\'(.+)\'\)/
            const backgroundImg = element[i].getAttribute('style').match(regexBackgroundUrl)[1];
            return {
                image: {
                href: element[i].getAttribute('href'),
                src: backgroundImg
                }
            };
            }
        }
        return {'image': ''}
    }
    ''')
    return photos


async def getTextContent(frame):
    selector = ".tgme_widget_message_text"
    content = await frame.querySelectorAllEval(selector, '''
    element => {
        if (element.length > 0) {
            for (i = 0; i < element.length; i++) {
            return {
                content: element[i].innerText
            };
        }
    }
    return {'content': ''};
    }
    ''')
    return content


async def getAvatar(frame):
    selector = ".tgme_widget_message_user_photo > img"
    avatar = await frame.querySelectorEval(selector, '''
        element => {
            return {'avatar': element.getAttribute('src')}
        }
    ''')
    return avatar


async def getUsername(frame):
    selector = ".tgme_widget_message_owner_name > span"
    username = await frame.querySelectorEval(selector, '''
        element => {
            return {'username': element.textContent}
        }
    ''')
    return username


asyncio.get_event_loop().run_until_complete(main())
