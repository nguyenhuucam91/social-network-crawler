import asyncio
from pyppeteer import launch
import re
import requests
import json

url = 'http://127.0.0.1:5500/socials/facebook.html'


async def main():
    browser = await launch({'headless': True})
    page = await browser.newPage()
    await page.goto(url, {
        'waitUntil': 'load',
        'timeout': 0

    })

    output = []

    for frameElement in page.mainFrame.childFrames:
        mergedVideoData = dict()
        facebookAvatar = await getUserAvatar(frameElement)
        facebookUser = await getUserInfo(frameElement)
        facebookPostedAt = await getPostedTime(frameElement)
        facebookContent = await getContent(frameElement)
        facebookImages = await getImages(frameElement)
        facebookVideos = await getVideos(frameElement)
        facebookLinks = await getLinks(frameElement)
        mergedVideoData.update(facebookAvatar)
        mergedVideoData.update(facebookUser)
        mergedVideoData.update(facebookPostedAt)
        mergedVideoData.update(facebookContent)
        mergedVideoData.update(facebookImages)
        mergedVideoData.update(facebookVideos)
        mergedVideoData.update(facebookLinks)
        output.append(mergedVideoData)

    print(output)

    await browser.close()


async def getUserAvatar(frame):
    userAvatar = await frame.querySelectorEval(
        '''
            ._3rt8._8o._8t.lfloat._ohe > img,
            ._92bm > img,
            ._38vo > div > img
        ''',
        '''
            ele => {
                return {'avatar': ele.getAttribute('src')}
            }
        '''
    )
    return userAvatar


async def getUserInfo(frame):
    username = await frame.querySelectorEval(
        '''
            ._2_79._50f4._50f7,
            ._2_79._50f7,
            ._90h_ > a
        ''',
        '''
            ele => {
                return {'username': ele.textContent}
            }
        '''
    )
    return username


async def getPostedTime(frame):
    selector = 'abbr.timestamp'
    postedTime = await frame.querySelector(selector)
    if postedTime is not None:
        return await frame.querySelectorEval(selector, '''
            element => {
                return {'posted_at': element.getAttribute('title')}
            }
        ''')
    else:
        return {'posted_at': ''}

    return postedTime


async def getContent(frame):
    content = await frame.querySelectorAllEval(
        '''
            div.userContent > div.text_exposed_root > p,
            div.userContent > div.text_exposed_root > div.text_exposed_show > p,
            div.userContent > p
        ''',
        '''
            ele => {
                facebookContent = ""
                for (i = 0; i < ele.length; i++) {
                    facebookContent += ele[i].textContent
                }
                return {content: facebookContent}
            }
        '''
    )
    return content


async def getImages(frame):
    image = await frame.querySelectorAllEval(
        '''
            ._4-u2._5v3q > div._2l7q > a > img,
            ._4-u2._5v3q > div > div > div._53j5._37u6 > div:nth-child(2) > img,
            .scaledImageFitWidth.img,
            .scaledImageFitHeight.img
        ''',
        '''
        ele => {
            const images = [];

            for (i = 0; i < ele.length; i++) {
                images.push(ele[i].getAttribute('src'));
            }
            return {images: images}
        }
        '''
    )
    return image


async def getVideos(frame):
    videos = await frame.querySelectorAllEval(
        '''
            ._4-u2._5v3q > div > div > div._53j5._37u6 > video
        ''',
        '''
        ele => {
            const videos = [];

            for (i = 0; i < ele.length; i++) {
                videos.push(ele[i].getAttribute('src'));
            }
            return {'videos': videos}
        }
        '''
    )
    return videos


async def getLinks(frame):
    links = await frame.querySelectorAllEval(
        '''
            div._2l7q > a,
            div._3ekx._29_4 > a
        ''',
        '''
        ele => {
            const links = [];

            for (i = 0; i < ele.length; i++) {
                linkHref = ele[i].getAttribute('href');
                if (ele[i].getAttribute('href').charAt(0) == '/') {
                    linkHref = "https://www.facebook.com" + ele[i].getAttribute('href')
                }
                links.push(linkHref);
            }
            return {links: links}
        }
        '''
    )
    return links


asyncio.get_event_loop().run_until_complete(main())
