import asyncio
from pyppeteer import launch
import re
import requests
import json

url = 'http://127.0.0.1:5500/socials/tiktok.html'


async def main():
    browser = await launch({'headless': True})
    page = await browser.newPage()
    await page.goto(url, {
        'waitUntil': 'networkidle0',
        'timeout': 0
    })

    output = []

    for frameElement in page.mainFrame.childFrames:
        mergedVideoData = dict()
        videoInfo = await getVideo(frameElement)
        avatar = await getAvatar(frameElement)
        username = await getUsername(frameElement)
        content = await getContent(frameElement)
        mergedVideoData.update(videoInfo)
        mergedVideoData.update(avatar)
        mergedVideoData.update(username)
        mergedVideoData.update(content)
        output.append(mergedVideoData)

    print(output)

    await browser.close()


async def getVideo(frame):
    video = await frame.querySelectorEval('div._embed_player_video-wrapper>video', '''
    (element) => {
        return {
            video: {
                src: element.getAttribute('src'),
                thumbnail: element.getAttribute('p')
            }
        }
    }
    ''')
    return video


async def getAvatar(frame):
    selector = "a._embed_video_info-avatar"
    avatar = await frame.querySelectorEval(selector, '''
    (element) => {
        return {
            avatar: element.getAttribute('style').match(/background-image:url[(](.+)[)]/)[1]
        }
    }
    ''')

    return avatar


async def getUsername(frame):
    selector = "a._embed_video_card-user"
    username = await frame.querySelectorEval(selector, '''
    (element) => {
        return {
            username: element.innerText.match(/@(\w+)/)[1]
        }
    }
    ''')

    return username


async def getContent(frame):
    selector = "._embed_video_card-text > span"
    content = await frame.querySelectorEval(selector, '''
    (element) => {
        return {
            content: element.innerText
        }
    }
    ''')

    return content

asyncio.get_event_loop().run_until_complete(main())
