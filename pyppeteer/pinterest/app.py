import asyncio
from pyppeteer import launch
import re
import requests
import json

url = 'http://127.0.0.1:5500/socials/pinterest.html'
pinHrefDomain = 'https://www.pinterest.com/pin/'


async def main():
    browser = await launch({'headless': True})
    page = await browser.newPage()
    await page.goto(url, {
        'waitUntil': 'networkidle0',
        'timeout': 0
    })

    pinImages = await getPinImage(page)

    embededPinInfo = []

    for pinImage in pinImages:
        pinId = pinImage.get('pin_id')
        pinText = await getPinText(page, pinId)
        pinVideo = await getPinVideo(page, pinId)
        pinUserAvatar = await getPinUserAvatar(page, pinId)
        pinUsername = await getPinUsername(page, pinId)
        pinImage.update(pinText)
        pinImage.update(pinVideo)
        pinImage.update(pinUserAvatar)
        pinImage.update(pinUsername)

        embededPinInfo.append(pinImage)

    print(embededPinInfo)


def getPinUrl(pinId):
    return pinHrefDomain + pinId + "/"


async def getPinImage(page):
    selector = '''
    span[class*=_image][class*=_coverMe],
    video[playsinline='playsinline']
    '''

    getPinImage = await page.querySelectorAllEval(selector, '''
        ele => {
        const output = [];
        for (i = 0; i < ele.length; i++) {
            if (ele[i].tagName == "VIDEO") {
                output.push({
                    pin_href: ele[i].parentNode.getAttribute('data-pin-href'),
                    img_src: ele[i].getAttribute('poster'),
                    pin_id: ele[i].parentNode.getAttribute('data-pin-href').match(/https:\/\/www.pinterest.com\/pin\/(\d+)/)[1]
                })
            } else {
                output.push({
                    pin_href: ele[i].getAttribute('data-pin-href'),
                    img_src: ele[i].getAttribute('style').match(/background-image: url[(]"(.+)"[)]/)[1],
                    pin_id: ele[i].getAttribute('data-pin-href').match(/https:\/\/www.pinterest.com\/pin\/(\d+)/)[1]
                })
            }
        }
        return output;
        }
        ''')
    return getPinImage


async def getPinUserAvatar(page, pinId):
    selector = f"""
        span[data-pin-log='embed_pin'][data-pin-id="{pinId}"] > span[data-pin-log='embed_pin_follow'] >  
        span[data-pin-log='embed_pin_follow'] > span:nth-child(2)
    """

    pinText = await page.querySelector(selector)
    if pinText is not None:
        return await page.querySelectorEval(selector,   '''
        ele => {
            return {'pin_avatar': ele.getAttribute('style').match(/background-image: url[(]"(.+)"[)]/)[1] }
        }
    '''
                                            )
    else:
        return {'pin_avatar': ''}


async def getPinUsername(page, pinId):
    selector = f"""
        span[data-pin-log='embed_pin'][data-pin-id="{pinId}"] > span[data-pin-log='embed_pin_follow'] >  
        span[data-pin-log='embed_pin_follow'] > span:nth-child(3) > span
    """

    return await page.querySelectorAllEval(selector, '''
    element => {
        for (i=0; i < element.length; i++) {
            if (element[i].textContent == "Published by") {
                return {'username': element[i+1].textContent}
            } else {
                return {'username': element[i].textContent}
            }
        }
    }
    ''')


async def getPinText(page, pinId):
    selector = f"""
        span[data-pin-log='embed_pin'][data-pin-id="{pinId}"] > span[data-pin-log='embed_pin_follow'] >  
        span[data-pin-log='embed_pin_follow'] > span:first-child
    """

    pinText = await page.querySelector(selector)
    if pinText is not None:
        return await page.querySelectorEval(selector,   '''
        ele => {
            return {'content': ele.textContent }
        }
    '''
                                            )

    else:
        return {'content': ''}


async def getPinVideo(page, pinId):
    selector = "span[data-pin-log='embed_pin'][data-pin-href='" + \
        getPinUrl(pinId) + "'] > video > source[type='video/mp4']"

    pinVideo = await page.querySelector(selector)
    if pinVideo is not None:
        return await page.querySelectorEval(selector, "(element) => { return {'video': element.getAttribute('src')} }")
    else:
        return {'video': ''}

asyncio.get_event_loop().run_until_complete(main())
