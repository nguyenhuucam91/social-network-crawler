const puppeteer = require("puppeteer");

const url = `file:///Volumes/Major/Practice/Crawler/socials/tiktok.html`;

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process'
    ]
  });
  const page = await browser.newPage();
  await page.goto(`${url}`, {
    waitUntil: 'networkidle0',
    timeout: 0
  });

  let output = [];

  for (const frameElement of page.mainFrame().childFrames()) {
    const videoInfo = await getVideo(frameElement)
    const avatar = await getAvatar(frameElement)
    const username = await getUsername(frameElement)
    const content = await getContent(frameElement)
    const tiktok = Object.assign(videoInfo, avatar, username, content)
    output.push(tiktok)
  }

  console.log(output);
  await browser.close();
})()


async function getVideo(frame) {
  const selector = `div._embed_player_video-wrapper > video`
  const video = await frame.$eval(selector,
    (element) => {
      return {
        'video': {
          'src': element.getAttribute('src'),
          'thumbnail': element.getAttribute('poster')
        }
      }
    })
  return video
}


async function getAvatar(frame) {
  const selector = "a._embed_video_info-avatar"
  avatar = await frame.$eval(selector,
    (element) => {
      return {
        'avatar': element.getAttribute('style').match(/background-image:url[(](.+)[)]/)[1]
      }
    }
  )

  return avatar
}

async function getUsername(frame) {
  const selector = "a._embed_video_card-user"
  return await frame.$eval(selector,
    (element) => {
      return {
        'username': element.innerText.match(/@(\w+)/)[1]
      }
    }
  )
}


async function getContent(frame) {
  const selector = "._embed_video_card-text > span"
  content = await frame.$eval(selector,
    (element) => {
      return {
        'content': element.innerText
      }
    }
  )
  return content
}