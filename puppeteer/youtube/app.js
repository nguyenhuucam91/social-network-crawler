const puppeteer = require("puppeteer");
const puppeteerConfig = require("../config/puppeteer")


const url = `file:///Volumes/Major/Practice/Crawler/socials/youtube.html`;

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process'
    ]
  });
  const page = await browser.newPage();
  await page.goto(`${url}`, {
    waitUntil: puppeteerConfig.waitUntil.load
  });

  const output = [];
  for (const frame of page.mainFrame().childFrames()) {
    const youtubeAvatar = await getYoutubeAvatar(frame)
    const youtubeUsername = await getYoutubeUser(frame)
    const youtubeVideoTitle = await getYoutubeVideoTitle(frame)
    const youtubeVideoThumbnail = await getYoutubeThumbnail(frame)
    const youtube = Object.assign(youtubeAvatar, youtubeUsername, youtubeVideoTitle, youtubeVideoThumbnail)
    output.push(youtube)
  }

  console.log(output)

  await browser.close();
})()

async function getYoutubeAvatar(frame) {
  const selector = `a.ytp-title-channel-logo`
  return await frame.$eval(selector,
    element => {
      return {
        avatar: element.getAttribute('style').match(/background-image: url[(]"(.+)"[)]/)[1],
      }
    })
}

async function getYoutubeUser(frame) {
  const selector = `h2.ytp-title-expanded-title > a`
  return await frame.$eval(selector,
    element => {
      return {
        username: element.innerText,
      }
    })
}

async function getYoutubeVideoTitle(frame) {
  const selector = `a.ytp-title-link`
  return await frame.$eval(selector,
    element => {
      return {
        title: element.textContent,
      }
    })
}

async function getYoutubeThumbnail(frame) {
  const selector = `.ytp-cued-thumbnail-overlay-image`
  return await frame.$eval(selector,
    element => {
      return {
        thumbnail: element.getAttribute('style').match(/background-image: url[(]"(.+)"[)]/)[1],
      }
    })
}