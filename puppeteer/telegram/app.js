const puppeteer = require("puppeteer");

const url = `file:///Volumes/Major/Practice/Crawler/socials/telegram.html`;

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process'
    ]
  });
  const page = await browser.newPage();
  await page.goto(`${url}`, {
    waitUntil: 'load',
    timeout: 0
  });

  let output = [];
  for (const frameElement of page.mainFrame().childFrames()) {
    photo = await getPhoto(frameElement)
    textContent = await getTextContent(frameElement)
    avatar = await getAvatar(frameElement)
    username = await getUsername(frameElement)
    let telegramPost = Object.assign(photo, textContent, avatar, username)
    output.push(telegramPost)
  }

  console.log(output);
  await browser.close();
})()

async function getPhoto(frame) {
  const selector = ".tgme_widget_message_photo_wrap"
  return await frame.$$eval(selector,
    element => {
      if (element.length > 0) {
        for (i = 0; i < element.length; i++) {
          const regexBackgroundUrl = /background[-]image:url\(\'(.+)\'\)/
          const backgroundImg = element[i].getAttribute('style').match(regexBackgroundUrl)[1];
          return {
            image: {
              href: element[i].getAttribute('href'),
              src: backgroundImg
            }
          };
        }
      }
      return {
        'image': ''
      }
    }
  )
}


async function getTextContent(frame) {
  const selector = ".tgme_widget_message_text"
  const content = await frame.$$eval(selector,
    element => {
      if (element.length > 0) {
        for (i = 0; i < element.length; i++) {
          return {
            'content': element[i].innerText
          };
        }
      }
      return {
        'content': ''
      };
    }
  )
  return content
}

async function getAvatar(frame) {
  const selector = ".tgme_widget_message_user_photo > img"
  const avatar = await frame.$eval(selector,
    element => {
      return {
        'avatar': element.getAttribute('src')
      }
    })
  return avatar
}


async function getUsername(frame) {
  const selector = ".tgme_widget_message_owner_name > span"
  const username = await frame.$eval(selector,
    element => {
      return {
        'username': element.textContent
      }
    })
  return username
}