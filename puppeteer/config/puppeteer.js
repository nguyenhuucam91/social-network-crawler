const puppeteerConfig = {

  waitUntil: {
    networkidle0: 'networkidle0',
    load: 'load',
    domcontentloaded: 'dom-content-loaded',
    networkidle2: 'networkidle2'
  }
}

module.exports = puppeteerConfig