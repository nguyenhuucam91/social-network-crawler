const puppeteer = require("puppeteer");
const puppeteerConfig = require('../config/puppeteer')

const url = `http://127.0.0.1:8080/socials/twitter.html`;

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process'
    ]
  });
  const page = await browser.newPage();
  await page.goto(`${url}`, {
    waitUntil: puppeteerConfig.waitUntil.networkidle0,
  });

  console.log(await page.content())
  // await page.waitForFunction('document.querySelector("blockquote.twitter-tweet")')


  // let output = [];
  // console.log(await page.content())
  // const twitterText = await getText(page);

  await browser.close();
})()

async function getText(page) {
  const text = await page.$eval('blockquote.twitter-tweet', element => {
    return element.outerHTML
  })

  console.log(text);
}