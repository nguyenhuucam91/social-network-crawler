const puppeteer = require("puppeteer");

const url = `file:///Volumes/Major/Practice/Crawler/socials/pinterest.html`;
const pinHrefDomain = "https://www.pinterest.com/pin/";

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process'
    ]
  });
  const page = await browser.newPage();
  await page.goto(`${url}`, {
    waitUntil: 'networkidle0',
    timeout: 0
  });

  const pinImages = await getPinImage(page)

  const embededPinInfo = []

  for (const pinImage of pinImages) {
    const pinId = pinImage.pin_id
    const pinText = await getPinText(page, pinId)
    const pinVideo = await getPinVideo(page, pinId)
    const pinUserAvatar = await getPinUserAvatar(page, pinId)
    const pinUsername = await getPinUsername(page, pinId)
    let pin = Object.assign(pinText, pinImage, pinVideo, pinUserAvatar, pinUsername)
    embededPinInfo.push(pin)
  }

  console.log(embededPinInfo)

  await browser.close();
})()

function getPinUrl(pinId) {
  return pinHrefDomain + pinId + "/"
}


async function getPinImage(page) {
  const selector =
    `
    span[class *= _image][class *= _coverMe],
    video[playsinline = 'playsinline']
  `;

  getPinImage = await page.$$eval(selector,
    ele => {
      const output = [];
      for (i = 0; i < ele.length; i++) {
        if (ele[i].tagName == "VIDEO") {
          output.push({
            pin_href: ele[i].parentNode.getAttribute('data-pin-href'),
            img_src: ele[i].getAttribute('poster'),
            pin_id: ele[i].parentNode.getAttribute('data-pin-href').match(/https:\/\/www.pinterest.com\/pin\/(\d+)/)[1]
          })
        } else {
          output.push({
            pin_href: ele[i].getAttribute('data-pin-href'),
            img_src: ele[i].getAttribute('style').match(/background-image: url[(]"(.+)"[)]/)[1],
            pin_id: ele[i].getAttribute('data-pin-href').match(/https:\/\/www.pinterest.com\/pin\/(\d+)/)[1]
          })
        }
      }
      return output;
    })
  return getPinImage
}



async function getPinUserAvatar(page, pinId) {
  const selector =
    `
      span[data-pin-log='embed_pin'][data-pin-id="${pinId}"] > span[data-pin-log='embed_pin_follow'] >  
      span[data-pin-log='embed_pin_follow'] > span:nth-child(2)
    `

  pinText = await page.$(selector)
  if (pinText != null) {
    return await page.$eval(selector,
      ele => {
        return {
          'pin_avatar': ele.getAttribute('style').match(/background-image: url[(]"(.+)"[)]/)[1]
        }
      }
    )
  } else {
    return {
      'pin_avatar': ''
    }
  }
}


async function getPinUsername(page, pinId) {
  const selector =
    `
      span[data-pin-log='embed_pin'][data-pin-id="${pinId}"] > span[data-pin-log='embed_pin_follow'] >  
      span[data-pin-log='embed_pin_follow'] > span:nth-child(3) > span
  `

  return await page.$$eval(selector,
    element => {
      for (i = 0; i < element.length; i++) {
        if (element[i].textContent == "Published by") {
          return {
            'username': element[i + 1].textContent
          }
        } else {
          return {
            'username': element[i].textContent
          }
        }
      }
    })
}

async function getPinText(page, pinId) {
  const selector =
    `
      span[data-pin-log='embed_pin'][data-pin-id="${pinId}"] > span[data-pin-log='embed_pin_follow'] >  
      span[data-pin-log='embed_pin_follow'] > span:first-child
    `

  pinText = await page.$(selector)
  if (pinText != null) {
    return await page.$eval(selector,
      ele => {
        return {
          'content': ele.textContent
        }
      }
    )
  } else {
    return {
      'content': ''
    }
  }
}

async function getPinVideo(page, pinId) {
  const selector = `
  span[data-pin-log='embed_pin'][data-pin-href='${getPinUrl(pinId)}'] > video > source[type='video/mp4']
  `

  pinVideo = await page.$(selector)
  if (pinVideo != null) {
    return await page.$eval(selector, "(element) => { return {'video': element.getAttribute('src')} }")
  } else {
    return {
      'video': ''
    }
  }
}