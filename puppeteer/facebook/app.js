const puppeteer = require("puppeteer");

const url = `file:///Volumes/Major/Practice/Crawler/socials/facebook.html`;

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--disable-web-security',
      '--disable-features=IsolateOrigins,site-per-process'
    ]
  });
  const page = await browser.newPage();
  await page.goto(`${url}`, {
    waitUntil: 'load',
    timeout: 0
  });

  const output = [];
  for (const frameElement of page.mainFrame().childFrames()) {
    const facebookAvatar = await getUserAvatar(frameElement)
    const facebookUser = await getUserInfo(frameElement)
    const facebookPostedAt = await getPostedTime(frameElement)
    const facebookContent = await getContent(frameElement)
    const facebookImages = await getImages(frameElement)
    const facebookVideos = await getVideos(frameElement)
    const facebookLinks = await getLinks(frameElement)
    let facebookPost = Object.assign(facebookAvatar, facebookUser, facebookPostedAt, facebookContent, facebookImages, facebookVideos, facebookLinks)
    output.push(facebookPost)
  }
  console.log(output)
  await browser.close();
})()


async function getUserAvatar(frame) {
  userAvatar = await frame.$eval(
    `
      ._3rt8._8o._8t.lfloat._ohe > img,
      ._92bm > img,
      ._38vo > div > img
    `,
    ele => {
      return {
        'avatar': ele.getAttribute('src')
      }
    }
  )
  return userAvatar
}


async function getUserInfo(frame) {
  username = await frame.$eval(
    `
    ._2_79._50f4._50f7,
    ._2_79._50f7,
    ._90h_ > a
    `,
    ele => {
      return {
        'username': ele.textContent
      }
    }
  )
  return username
}


async function getPostedTime(frame) {
  selector = 'abbr.timestamp'
  postedTime = await frame.$(selector)
  if (postedTime != null) {
    return await frame.$eval(selector,
      element => {
        return {
          'posted_at': element.getAttribute('title')
        }
      })
  } else {
    return {
      'posted_at': ''
    }
  }
}



async function getContent(frame) {
  const content = await frame.$$eval(
    `
    div.userContent > div.text_exposed_root > p,
    div.userContent > div.text_exposed_root > div.text_exposed_show > p,
    div.userContent > p 
    `,
    ele => {
      facebookContent = ""
      for (i = 0; i < ele.length; i++) {
        facebookContent += ele[i].textContent
      }
      return {
        'content': facebookContent
      }
    }
  )
  return content
}


async function getImages(frame) {
  image = await frame.$$eval(
    `
      ._4-u2._5v3q > div._2l7q > a > img,
      ._4-u2._5v3q > div > div > div._53j5._37u6 > div:nth-child(2) > img,
      .scaledImageFitWidth.img,
      .scaledImageFitHeight.img
    `,
    ele => {
      const images = [];

      for (i = 0; i < ele.length; i++) {
        images.push(ele[i].getAttribute('src'));
      }
      return {
        'images': images
      }
    }
  )
  return image
}

async function getVideos(frame) {
  videos = await frame.$$eval(
    `  
      ._4-u2._5v3q > div > div > div._53j5._37u6 > video
    `,
    ele => {
      const videos = [];

      for (i = 0; i < ele.length; i++) {
        videos.push(ele[i].getAttribute('src'));
      }
      return {
        'videos': videos
      }
    }
  )
  return videos
}



async function getLinks(frame) {
  links = await frame.$$eval(
    `
    div._2l7q > a,
    div._3ekx._29_4 > a 
    `,
    ele => {
      const links = [];

      for (i = 0; i < ele.length; i++) {
        linkHref = ele[i].getAttribute('href');
        if (ele[i].getAttribute('href').charAt(0) == '/') {
          linkHref = "https://www.facebook.com" + ele[i].getAttribute('href')
        }
        links.push(linkHref);
      }
      return {
        'links': links
      }
    }
  )
  return links
}