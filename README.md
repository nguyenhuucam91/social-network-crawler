# If running in python

## Crawl data from embeded page

In folder `socials`, there are test files from different social networks, use this as test item

## Introduction

This crawler is written in python and using pyppeteer as crawl engine

## Environment requirements

- python >3.6
- live-server: Since python need access to http URL instead of file:///

## Installation instructions

Install the following libraries after install python. Running these commands to install

- `pip3 install requests`
- `pip3 install pyppeteer`

Install ffmpeg for merging sound and video

- `sudo apt-get install ffmpeg`

## Reference link

- <https://github.com/ytdl-org/youtube-dl>

# If running in NodeJS

- Simply run `npm install` to install neccesary package
